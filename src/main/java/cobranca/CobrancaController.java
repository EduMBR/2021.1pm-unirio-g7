package cobranca;

import java.util.Objects;

import cartaoDeCredito.CartaoDeCredito;
import cartaoDeCredito.CartaoDeCreditoController;
import io.javalin.http.Context;
import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;



public class CobrancaController {
	
	CobrancaController() {
		
	}
	 public static void getCobranca (Context ctx) {
	        String idCobranca = Objects.requireNonNull(ctx.pathParam("id"));		 	
	        CobrancaDAO dao = new CobrancaDAO();	       
	        Cobranca cobranca = dao.getCobrancaById(idCobranca);
	        
	        if (cobranca != null) {
	        	ctx.status(200);
	        	ctx.json(cobranca);
	        		        	
	        } else {
	        	ctx.html("Cobrança não encontrada");
	            ctx.status(404);
	            		
	        }
	    };
	    public static void realizarCobranca(Context ctx) {
	    	CobrancaForm cobrancaJson = ctx.bodyAsClass(CobrancaForm.class);
	    	Cobranca cobranca = cobrancaJson.desserializarPostCobranca();
	    	CartaoDeCreditoController cartaoController = new CartaoDeCreditoController();
	    	CartaoDeCredito cartaoRetornado = cartaoController.getCartaoDeCreditoIntegracao(cobranca.getCiclistaId());
	    	EnviadoraCobranca enviadora = new EnviadoraCobranca(cobranca, cartaoRetornado);
	    	HttpResponse<JsonNode> response = enviadora.enviarCobranca();
	    	ctx.json(response.getBody());
	    	
	    	if (response.getStatus() == 201) {
	    		 ctx.status(201);
	             return;
	         } 
	    	else {
	    		
	    		ctx.status(422);

		    }
	    }
	 
	  
};

