package cobranca;

import java.util.UUID;


public class Cobranca {
	
	private String id;
	private Status status;
	private String horaSolicitacao;
	private String horaFinalizacao;
	private String ciclistaId;
	private int valor;

	Cobranca(Status status, String horaSolicitacao, String horaFinalizacao, int valor, String ciclistaId){
		this.id = UUID.randomUUID().toString();
		this.status = status;
		this.horaSolicitacao = horaSolicitacao;
		this.horaFinalizacao = horaFinalizacao;
		this.valor = valor;
		this.ciclistaId = ciclistaId;
	}
	Cobranca(String id, Status status, String horaSolicitacao, String horaFinalizacao, int valor, String ciclistaId){
		this.id = id;
		this.status = status;
		this.horaSolicitacao = horaSolicitacao;
		this.horaFinalizacao = horaFinalizacao;
		this.valor = valor;
		this.ciclistaId = ciclistaId;
	}
	public Cobranca(String ciclistaId,  int valor){
		this.id = UUID.randomUUID().toString();
		this.valor = valor;
		this.ciclistaId = ciclistaId;
	}

	public String getId() {
		return id;
	}

	public String getHoraSolicitacao() {
		return horaSolicitacao;
	}

	public void setHoraSolicitacao(String horaSolicitacao) {
		this.horaSolicitacao = horaSolicitacao;
	}

	public String getHoraFinalizacao() {
		return horaFinalizacao;
	}

	public void setHoraFinalizacao(String horaFinalizacao) {
		this.horaFinalizacao = horaFinalizacao;
	}

	public String getCiclistaId() {
		return ciclistaId;
	}

	public void setCiclistaId(String ciclistaId) {
		this.ciclistaId = ciclistaId;
	}

	public int getValor() {
		return valor;
	}

	public void setValor(int valor) {
		this.valor = valor;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

}
