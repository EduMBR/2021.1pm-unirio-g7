package cobranca;

import java.util.ArrayList;

public class CobrancaDAO {
	 		   

	CobrancaDAO() {
	
    }
	
    
    public ArrayList<Cobranca> retornarCobrancas() {
    	ArrayList<Cobranca> cobrancas = new ArrayList<Cobranca>();
    	cobrancas.add(new Cobranca("3fa85f64-5717-4562-b3fc-2c963f66afa6",  1));
    	cobrancas.add(new Cobranca("5fc808e4-d59d-4556-8ee7-b2d754b8cdf1", Status.PENDENTE, "2021-09-03T13:23:41.414Z","2021-09-03T13:25:29.195Z", 7, "3fa85f64-5717-4562-b3fc-2c963f66afa7"));
    	return cobrancas;
    }

    Cobranca getCobrancaById(String id) {
        ArrayList<Cobranca> cobrancas = retornarCobrancas();
        
        try {
	        for (int i = 0; i < cobrancas.size(); i++) {	        	
				if(cobrancas.get(i).getId().equals(id)) {
					return cobrancas.get(i);
				}
		}
        }catch(NullPointerException n) {
        	return null;        
        }
		return null;
        
        }
    }
        