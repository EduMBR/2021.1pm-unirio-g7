package cobranca;

import io.javalin.http.Context;
import kong.unirest.Unirest;


public class FilaCobrancaController {
	
	private FilaCobrancaController() {
		
	}
	public static void postFilaCobranca (Context ctx){
    	
		try {
			CobrancaForm cobrancaJson = ctx.bodyAsClass(CobrancaForm.class);
	    	Cobranca cobranca = cobrancaJson.desserializar();
			FilaCobrancaDAO dao = new FilaCobrancaDAO();	    	    	
	    	FilaCobranca filaCobranca = dao.retornarFilaCobrancaPreenchida();
	    	filaCobranca.setFilaCobranca(cobranca);
	    	ctx.status(200);
	    	ctx.json(filaCobranca);
		}catch(Exception ex) {
			
			ctx.status(422);
		}
		
    };
    

  	public void removerFilaCobranca() {
  		FilaCobrancaDAO dao = new FilaCobrancaDAO();
  		FilaCobranca fila = dao.retornarFilaCobrancaPreenchida();
  		Cobranca cobrancaRemovida = fila.getFilaCobranca().remove();
  		EnviadoraCobranca cobrar = new EnviadoraCobranca();
  		cobrar.setCobranca(cobrancaRemovida);
  		cobrar.enviarCobranca();  		
  	}
}