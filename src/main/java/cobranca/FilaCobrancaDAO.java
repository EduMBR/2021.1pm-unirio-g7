package cobranca;


public class FilaCobrancaDAO {
	
	private FilaCobranca filaCobranca = new FilaCobranca();
	
  	public FilaCobranca retornarFilaCobrancaPreenchida() {
  		FilaCobranca filaCobrancas = new FilaCobranca();
  		filaCobrancas.getFilaCobranca().add(new Cobranca(Status.PENDENTE, "2019-09-03T13:23:41.414Z","2019-09-03T13:25:29.195Z", 10, "3fa85f62-5717-4562-b3fc-2c963f66afa8")) ;  		
  		return filaCobrancas;  		
  	}
  
  	public FilaCobranca getFilaCobranca() {
		return filaCobranca;
	}
 
}
