package cobranca;

import java.util.LinkedList;
import java.util.Queue;

public class FilaCobranca {
	
	public FilaCobranca() {
		
	}

	public FilaCobranca(Queue<Cobranca> filaCobranca) {
		for (Cobranca cobranca : filaCobranca) {
			this.filaDeCobranca.add(cobranca);
		}
		
	}

	private Queue<Cobranca> filaDeCobranca = new LinkedList<>();
	
	

	public Queue<Cobranca> getFilaCobranca() {
		return filaDeCobranca;
	}

	public void setFilaCobranca(Cobranca cobranca) {
		this.filaDeCobranca.add(cobranca);
	}
	
	    
}
