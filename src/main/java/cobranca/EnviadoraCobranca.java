package cobranca;


import cartaoDeCredito.CartaoDeCredito;
import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;

public class EnviadoraCobranca {

	private static final String MERCHANTID = "399cdf44-a69b-4522-8d39-b0e717b693eb";
	private static final String MERCHANTKEY = "FGYOFXBXBYNORQQSFDKMVMCGMLXENNBINGKOBKFS";
	private static final String REQUISICAOCIELO = "https://apisandbox.cieloecommerce.cielo.com.br/1/sales/";
	private Cobranca cobranca;
	private CartaoDeCredito cartao;
	
	public EnviadoraCobranca (){
		
	}
	
	public EnviadoraCobranca (Cobranca cobranca, CartaoDeCredito cartao){
		this.cobranca = cobranca;
		this.cartao = cartao;
	}
	
	public HttpResponse<JsonNode> enviarCobranca() {
		TransacaoCielo transacao = new TransacaoCielo("1");
		transacao.Customer.Name = this.cartao.getNomeTitular();
		transacao.Payment.setAmount(cobranca.getValor());
		transacao.Payment.setInstallments(1);
		transacao.Payment.setType("CreditCard");
		transacao.Payment.getCreditCard().setBrand("Visa");
		transacao.Payment.getCreditCard().setCardNumber(cartao.getNumero());
		transacao.Payment.getCreditCard().setExpirationDate(cartao.getValidade());
		transacao.Payment.getCreditCard().setSecurityCode(cartao.getCvv());
		HttpResponse<JsonNode>response = Unirest.post(REQUISICAOCIELO).header("Content-Type", "application/json").header("MerchantKey", MERCHANTKEY).header("MerchantId", MERCHANTID).body(transacao).asJson();
		return response;
	}
	
	public HttpResponse<JsonNode> validarCartaoDeCredito(CartaoDeCredito cartao){
		TransacaoCielo transacao = new TransacaoCielo("1");
		transacao.Payment.setAmount(1);
		transacao.Payment.setInstallments(1);
		transacao.Payment.setType("CreditCard");
		transacao.Payment.getCreditCard().setBrand("Visa");
		transacao.Payment.getCreditCard().setCardNumber(cartao.getNumero());
		transacao.Payment.getCreditCard().setExpirationDate(cartao.getValidade());
		transacao.Payment.getCreditCard().setSecurityCode(cartao.getCvv());
		HttpResponse<JsonNode>response = Unirest.post(REQUISICAOCIELO).header("Content-Type", "application/json").header("MerchantKey", MERCHANTKEY).header("MerchantId", MERCHANTID).body(transacao).asJson();
		return response;
	}
	
	public Cobranca getCobranca() {
		return cobranca;
	}
	

	public void setCobranca(Cobranca cobranca) {
		this.cobranca = cobranca;
	}

}
