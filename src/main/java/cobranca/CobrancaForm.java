package cobranca;

import java.util.UUID;

public class CobrancaForm {
	public String id;
	public String ciclistaId;
	public int valor;
	public String horaSolicitacao;
	public String horaFinalizacao;	
	public Status status;

	public CobrancaForm() {
		
	}
	
	CobrancaForm(String id, Status status, String horaSolicitacao, String horaFinalizacao, int valor, String ciclistaId){
		this.id = id;
		this.status = status;
		this.horaSolicitacao = horaSolicitacao;
		this.horaFinalizacao = horaFinalizacao;
		this.valor = valor;
		this.ciclistaId = ciclistaId;
	}
	public CobrancaForm(String ciclistaId,  int valor){
		this.id =  UUID.randomUUID().toString();
		this.valor = valor;
		this.ciclistaId = ciclistaId;
	}
	
	public Cobranca desserializar(){
		return new Cobranca(this.id, this.status, this.horaSolicitacao, this.horaFinalizacao, this.valor, this.ciclistaId);
	}
	public Cobranca desserializarPostCobranca(){
		return new Cobranca(this.ciclistaId, this.valor);
	}
}
