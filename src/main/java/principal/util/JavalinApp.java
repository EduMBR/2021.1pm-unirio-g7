package principal.util;
import email.EmailController;
import cartaoDeCredito.CartaoDeCreditoController;
import cobranca.CobrancaController;
import cobranca.FilaCobrancaController;
import io.javalin.Javalin;


import static io.javalin.apibuilder.ApiBuilder.*;

public class JavalinApp {
    private Javalin app = 
            Javalin.create(config -> config.defaultContentType = "application/json")
                .routes(() -> {
                   path("/getCobranca/:id", ()-> get(CobrancaController::getCobranca));
                   path("/filaCobranca",  () -> post(FilaCobrancaController::postFilaCobranca));		
                   path("/validaCartaoDeCredito",  ()-> post(CartaoDeCreditoController::validaCartaoCredito));
                   path("/enviarEmail",  ()-> post(EmailController::enviarEmail));
                   path("/cobranca",  ()-> post(CobrancaController::realizarCobranca));
                    });
                   

    public void start(int port) {
        this.app.start(port);
    }

    public void stop() {
        this.app.stop();
    }
}
