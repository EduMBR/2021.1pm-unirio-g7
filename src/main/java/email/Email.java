package email;
import java.util.UUID;

public class Email {

	public Email(String email, String mensagem) {
		
		this.id = UUID.randomUUID().toString();
		this.mensagem = mensagem;
		this.emailDestinatario = email;
		
	}
	
	private String id;
	private String mensagem;
	private String emailDestinatario;
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getEmail() {
		return emailDestinatario;
	}
	public void setEmail(String email) {
		this.emailDestinatario = email;
	}
	public String getMensagem() {
		return mensagem;
	}
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}


}
