package email;

import java.io.IOException;
import java.util.Properties;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;


import io.javalin.http.Context;

public class EmailController {

	private EmailController() {
		
	}
	
    public static void enviarEmail( Context ctx) throws MessagingException, IOException {
    	
    	Properties prop = new Properties();
    	prop.put("mail.smtp.auth", "true");
    	prop.put("mail.smtp.starttls.enable", "true");
    	prop.put("mail.smtp.ssl.protocols", "TLSv1.2");
    	prop.put("mail.smtp.host", "smtp.gmail.com");
    	prop.put("mail.smtp.port", "587");
    	
    	Session session = Session.getInstance(prop, new Authenticator() {
    		@Override
    		protected PasswordAuthentication getPasswordAuthentication() {
				
    			return new PasswordAuthentication( "sistemabicicletarioexterno@gmail.com", "PM12345678" );
    			
    		}
    	});
    	
    	Message mensagem = prepareMessage(session, ctx);
    	
    	Transport.send(mensagem);
    	ctx.status(200);
    }

	private static Message prepareMessage(Session session, Context ctx) throws IOException {
		Message mensagem = new MimeMessage(session);
		EmailForm emailJson = ctx.bodyAsClass(EmailForm.class);
    	Email email = emailJson.desserializar();    
		try {
			mensagem.setFrom(new InternetAddress("sistemabicicletarioexterno@gmail.com"));
			mensagem.setRecipient(Message.RecipientType.TO, new InternetAddress(email.getEmail()));
			mensagem.setSubject("Sistema Bicicletário");
			mensagem.setText(email.getMensagem());
			return mensagem;
			
		} catch (AddressException e) {
			ctx.status(404);
			e.printStackTrace();
		} catch (MessagingException e) {
			ctx.status(422);
			e.printStackTrace();
		}
		return null;
	}
    
}
