package email;

public class EmailForm {

	public EmailForm(String email, String mensagem) {
		this.mensagem = mensagem;
		this.email = email;
	}
	public EmailForm() {
	
	}
	
	public String email;
	public String mensagem;
	
	public Email desserializar() {
		
		return new Email(this.email, this.mensagem);
		
	}
}
