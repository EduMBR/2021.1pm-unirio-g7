package cartaoDeCredito;

public class CartaoDeCreditoForm {
	
	public String id;
	public String nomeTitular;
	public String cvv;
	public String validade;
	public String numero;
	
	public CartaoDeCreditoForm() {
		
	} 
	public CartaoDeCreditoForm( String nomeTitular,  String numero, String validade, String cvv){
		this.numero = numero;
		this.nomeTitular = nomeTitular;
		this.validade = validade;
		this.cvv = cvv;
	}

	public CartaoDeCredito desserializar(){
		return new CartaoDeCredito(this.nomeTitular,this.numero,this.validade, this.cvv);
	}
}
