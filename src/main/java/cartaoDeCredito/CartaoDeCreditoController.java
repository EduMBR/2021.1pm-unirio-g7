package cartaoDeCredito;

import cobranca.EnviadoraCobranca;
import io.javalin.http.Context;
import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import kong.unirest.json.JSONObject;

public class CartaoDeCreditoController {
	
	private final String caminhoIntegracao = "https://g11-pm.herokuapp.com/cartaoDeCredito/";
		
	public CartaoDeCreditoController() {
		//metodo vazio 
	}
	
	public static boolean validaCartaoCredito(Context ctx) {
		CartaoDeCredito cartao = ctx.bodyAsClass(CartaoDeCreditoForm.class).desserializar();
		EnviadoraCobranca validacao = new EnviadoraCobranca();
		HttpResponse<JsonNode> resultado = validacao.validarCartaoDeCredito(cartao);
		
		if(resultado.getStatus() == 201) {
			ctx.status(200);
			return true;
		}
		else if (resultado.getStatus() == 422){
			ctx.status(422);
			return false;
		}else if(resultado.getStatus() == 402){
			ctx.status(402);
			return false;
		}
		return false;
	}
	
	public CartaoDeCredito getCartaoDeCreditoIntegracao(String id) {
		//Aqui está a linha de código que realiza a integralçao com microsserviço aluguel
		HttpResponse<JsonNode> response = Unirest.get(caminhoIntegracao + id).header("accept", "application/json").asJson();		
		JSONObject json = response.getBody().getObject();
		CartaoDeCredito cartao = new CartaoDeCredito(json.getString("nomeTitular"),  json.getString("numero"), json.getString("validade"), json.getString("cvv"));
		return cartao;		
	}
  }
