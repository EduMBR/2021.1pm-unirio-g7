package cartaoDeCredito;

import java.util.UUID;

public class CartaoDeCredito {

	private String cvv;
	private String nomeTitular;
	private String numero;
	private String validade;
	private String id;

	public CartaoDeCredito( String nomeTitular,  String numero, String validade, String cvv){
		this.id = UUID.randomUUID().toString();
		this.cvv = cvv;
		this.nomeTitular = nomeTitular;
		this.numero = numero;
		this.validade = validade;
	}

	public String getCvv() {
		return cvv;
	}

	public void setCvv(String cvv) {
		this.cvv = cvv;
	}

	public String getNomeTitular() {
		return nomeTitular;
	}

	public void setNomeTitular(String nomeTitular) {
		this.nomeTitular = nomeTitular;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getValidade() {
		return validade;
	}

	public void setValidade(String validade2) {
		this.validade = validade2;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
