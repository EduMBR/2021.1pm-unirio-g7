package echo;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import io.javalin.http.Context;
import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import principal.util.JavalinApp;



class ControllerTest {
	
    private static JavalinApp app = new JavalinApp();
    
    @BeforeAll
    static void init() {
        app.start(7010);
    }
    
    @AfterAll
    static void afterAll(){
        app.stop();
    }
    @Test
    void getCobarancaTest() {    	         	
        HttpResponse response = Unirest.get("https://pm-unirio-g7.herokuapp.com/getCobranca/5fc808e4-d59d-4556-8ee7-b2d754b8cdf1").asString();
        assertEquals(200, response.getStatus());          
    }
    
    @Test
    void postEnviarEmailTest() {    	         	
       HttpResponse<JsonNode> response = Unirest.post("https://pm-unirio-g7.herokuapp.com/enviarEmail").body("{\"email\":\"eduardo.barboza@uniriotec.br\", \"mensagem\":\"Testanto o teste\"}").asJson();
       assertEquals(200, response.getStatus());        
    }

    @Test
    void postFilaCobrancaTest() {    	         	
    	HttpResponse<JsonNode> response = Unirest.post("https://pm-unirio-g7.herokuapp.com/filaCobranca").body("{\"status\":\"PAGA\", \"horaSolicitacao\":\"2020-09-03T13:23:41.414Z\", \"horaFinalizacao\":\"2020-09-03T13:25:29.195Z\", \"valor\":\"20\", \"ciclistaId\":\"3fa85f62-5717-4562-b3fc-2c963f66af72\"}").asJson();
    	assertEquals(200, response.getStatus());     
    }
    

    @Test
    void postCobrancaTest() {    	         	
       HttpResponse<JsonNode> response = Unirest.post("https://pm-unirio-g7.herokuapp.com/cobranca").body("{\"ciclistaId\":\"fad0bb10-7549-4ae2-916c-0c2e52e1021a\", \"valor\":\"1\"}").asJson();
       assertEquals(500, response.getStatus());     
    }
     
    @Test
    void postValidaCartaoDeCreditoTest() {    	          																					
       HttpResponse<JsonNode> response = Unirest.post("https://pm-unirio-g7.herokuapp.com/validaCartaoDeCredito").body("{\"numero\":\"0000.0000.0000.0001\", \"validade\":\"12/2030\", \"nomeTitular\": \"Testador123\", \"cvv\":\"123\"}").asJson();
       assertEquals(200, response.getStatus());     
    }
    
    //Precisa de um Id para funcionar
    @Test
    void getCartaoDeCreditoIntegracaoTest() {    	         	
       HttpResponse response = Unirest.get("https://g11-pm.herokuapp.com/cartaoDeCredito/8911a91f-9380-4024-aa18-8acab51dc6d7").asString();
       assertEquals(422, response.getStatus());     
    }
    
}
